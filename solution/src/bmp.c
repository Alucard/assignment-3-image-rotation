#include "bmp.h"
#include "status.h"
#include <stdio.h>
#include <stdlib.h>


#define BMP_TYPE 0x4D42
#define BMP_RESERVED 0
#define BI_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_X_PIXELS_PER_M 0
#define BMP_Y_PIXELS_PER_M 0
#define BMP_COLORS_USED 0
#define BMP_COLORS_IMPORTANT 0


long calculatePadding(uint32_t width) {
	uint32_t width_size = width * sizeof(struct pixel);
	return (long)(4 - (width_size % 4)) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header;

	if (!(fread(&header, sizeof(struct bmp_header), 1, in) == 1)) {
		free_image(img);
		return 	READ_INVALID_HEADER;
	}

	long padding = calculatePadding(header.biWidth);

	img->width = header.biWidth;
	img->height = header.biHeight;

	struct pixel* data = (struct pixel*)malloc(img->width*img->height*sizeof(struct pixel));

	if (!data) {
		return READ_OUT_OF_MEMORY;
	}

	if (fseek(in, (long)header.bOffBits, SEEK_SET)) {
		free(data);
		return READ_INVALID_PIXELS;
	}

	for (uint32_t i = 0; i < img->height; i++) {
		if (!(fread(data + i * img->width, sizeof(struct pixel), img->width, in) == img->width)){
			free_image(img);
			return READ_INVALID_PIXELS;
		}
		if (fseek(in, padding, SEEK_CUR)) {
			free_image(img);
			return READ_INVALID_PIXELS;
		}
	}
	
	img->data = data;

	return READ_OK;
}

static struct bmp_header init_bmp_header(uint64_t width, uint64_t height, long padding) {
	struct bmp_header header;

	uint32_t sizeImage = (width * sizeof(struct pixel) + padding) * height;
	uint32_t fileSize = sizeof(struct bmp_header) + sizeImage;

	header.bfType = BMP_TYPE;
	header.bfileSize = fileSize;
	header.bfReserved = BMP_RESERVED;
	header.bOffBits = sizeof(struct bmp_header);
	header.biSize = BI_SIZE;
	header.biWidth = width;
	header.biHeight = height;
	header.biPlanes = BMP_PLANES;
	header.biBitCount = BMP_BIT_COUNT;
	header.biCompression = BMP_COMPRESSION;
	header.biSizeImage = sizeImage;
	header.biXPelsPerMeter = BMP_X_PIXELS_PER_M;
	header.biYPelsPerMeter = BMP_Y_PIXELS_PER_M;
	header.biClrUsed = BMP_COLORS_USED;
	header.biClrImportant = BMP_COLORS_IMPORTANT;

	return header;
}


enum write_status to_bmp(FILE* out, struct image const* img) {
	long padding = calculatePadding(img->width);

	struct bmp_header header = init_bmp_header(img->width, img->height, padding);
	 
	if (!(fwrite(&header, sizeof(struct bmp_header), 1, out) == 1)) {
		return WRITE_HEADER_ERROR;
	}
	 
	char pad[sizeof(struct pixel)] = {0, 0, 0};

	for (uint32_t i = 0; i < img->height; i++) {
			if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) {
				return WRITE_DATA_ERROR;
			}
			if (fwrite(pad, sizeof(char), padding, out) != padding) {
				return WRITE_PADDING_ERROR;
			}
		
	}

	return WRITE_OK;
}
