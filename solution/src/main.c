#include "bmp.h"
#include "rotation.h"
#include "status.h"
#include <stdio.h>
#include <stdlib.h>

int is_angle_valid(int angle) {
	int required_angle_list[] = { -270, -180, -90, 0, 90, 180, 270 };
	for (int i = 0; i < sizeof(required_angle_list) / sizeof(required_angle_list[0]); i++) {
		if (angle == required_angle_list[i]){
			return 1;
		}
	}
	return 0;
}

int main(int argc, char* argv[]) {
	if (argc != 4) {
		fprintf(stderr, "Arguments error! Check if u run the program rightly: . / image - transformer <source - image> <transformed - image> <angle>\n");
		return 1;
	}

	int angle = atoi(argv[3]);
	if (!is_angle_valid(angle)) {
		fprintf(stderr, "Angle is invalid! Check if u use one of these angles: 0, 90, -90, 180, -180, 270, -270\n");
		return 1;
	}
	

	FILE* in = fopen(argv[1], "rb");
	if (!in) {
		fprintf(stderr, "Couldn't open file with name: %s\n", argv[1]);
		return 1;
	}

	struct image* img = (struct image*) malloc(sizeof(struct image));
	enum read_status r_status = from_bmp(in, img);
	if (r_status != 0) {
		fprintf(stderr,"Coudn't read image from file with name: %s\n", argv[1]);
		free_image(img);
		return 1;
	}

	struct image* rotated_img = rotate(img, angle);

	FILE* out = fopen(argv[2], "wb");
	if (!out) {
		fprintf(stderr, "Couldn't open file with name: %s\n", argv[2]);
		free_image(rotated_img);
		return 1;
	}

	enum write_status w_status = to_bmp(out, rotated_img);
	if (w_status != 0) {
		fprintf(stderr, "Coudn't write image to file with name: %s\n", argv[2]);
		free_image(rotated_img);
		return 1;
	}

	free_image(rotated_img);

	return 0;
}	
