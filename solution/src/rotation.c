#include "rotation.h"

struct image* rotate_90(struct image* img) {
	struct image* returning_image;
	returning_image = init_image(img->height, img->width);
	for (int i = 0; i < img->height; i++) { 
		for (int q = 0; q < img->width; q++) {
			returning_image->data[returning_image->width * (returning_image->height - q - 1) + i] = img->data[img->width * i + q];
		}
	}
	free_image(img);
	return returning_image;
}


struct image* rotate(struct image* img, int angle) {
	if (angle == 0) {
		return img;
	}
	if (angle < 0) {
		angle += 360;
	}
	int rotation_count =  angle / 90;

	struct image* returning_image = img;
	for (int i = 0; i < rotation_count; i++) {
		returning_image = rotate_90(returning_image);
	}

	return returning_image;
}
