#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct image* init_image(uint64_t width, uint64_t height) {
	struct image* img = (struct image*)malloc(sizeof(struct image));
	img->data = (struct pixel*)malloc(width * height * sizeof(struct pixel));
	img->width = width;
	img->height = height;
	return img;
}

void free_image(struct image* img) {
	if (img) {
		free(img->data);
		free(img);
	};
}
