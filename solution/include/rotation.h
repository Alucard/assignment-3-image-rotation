#ifndef ROTATION_H
#define ROTATION_H
#include "image.h"

struct image* rotate(struct image* img, int angle);

#endif
